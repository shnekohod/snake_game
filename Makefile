.PHONY: all clean install uninstall

TARGET = snake_game

all: $(TARGET)

%.o: src/%.c
	$(CC) $(CFLAGS) -c $< -o $@

$(TARGET): main.o game.o snake.o
	$(CC) $(CFLAGS) $^ -lSDL2 -o $(TARGET)

clean:
	rm -f $(TARGET) *.o

install:
	install $(TARGET) $(PREFIX)

uninstall:
	rm $(PREFIX)/$(TARGET)
